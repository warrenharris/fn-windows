# Run PowerShell with administrative privileges to execute these commands

# Define the rule names
$AllowHTTPSRuleName = "AllowOutboundHTTPS"
$AllowDNSRuleName = "AllowOutboundDNS"
$AllowSMTPRuleName = "AllowOutboundSMTP"

# Define the protocols (TCP for HTTPS and SMTP, UDP for DNS) and the specific ports (443 for HTTPS, 25 and 587 for 
SMTP, and 53 for DNS)
$HTTPSProtocol = "TCP"
$SMTPProtocol = "TCP"
$DNSProtocol = "UDP"
$HTTPSRemotePort = 443
$SMTPRemotePorts = "25,587"
$DNSRemotePort = 53

# Create a new rule to allow outbound HTTPS traffic on port 443
netsh advfirewall firewall add rule name=$AllowHTTPSRuleName dir=out action=allow protocol=$HTTPSProtocol 
remoteport=$HTTPSRemotePort

# Create a new rule to allow outbound DNS traffic on port 53
netsh advfirewall firewall add rule name=$AllowDNSRuleName dir=out action=allow protocol=$DNSProtocol 
remoteport=$DNSRemotePort

# Create a new rule to allow outbound SMTP traffic on ports 25 and 587
foreach ($port in $SMTPRemotePorts -split ',') {
    netsh advfirewall firewall add rule name="${AllowSMTPRuleName}-${port}" dir=out action=allow protocol=$SMTPProtocol 
remoteport=$port
}

Write-Host "Outbound HTTPS traffic is allowed, outbound DNS traffic is allowed, and outbound SMTP traffic is allowed on 
ports 25 and 587."

