# Run PowerShell with administrative privileges to execute these commands

# Define the rule names
$AllowHTTPSRuleName = "AllowOutboundHTTPS"
$AllowDNSRuleName = "AllowOutboundDNS"
$AllowSMTPRuleName = "AllowOutboundSMTP"

# Define the protocols (TCP for HTTPS and SMTP, UDP for DNS) and the specific ports (443 for HTTPS, 25 and 587 for 
SMTP, and 53 for DNS)
$HTTPSProtocol = 6  # 6 represents TCP
$SMTPProtocol = 6   # 6 represents TCP
$DNSProtocol = 17   # 17 represents UDP
$HTTPSRemotePort = 443
$SMTPRemotePorts = @(25, 587)
$DNSRemotePort = 53

# Create a new rule to allow outbound HTTPS traffic on port 443
New-NetFirewallRule -DisplayName $AllowHTTPSRuleName -Direction Outbound -Protocol $HTTPSProtocol -RemotePort 
$HTTPSRemotePort -Action Allow

# Create a new rule to allow outbound DNS traffic on port 53
New-NetFirewallRule -DisplayName $AllowDNSRuleName -Direction Outbound -Protocol $DNSProtocol -RemotePort 
$DNSRemotePort -Action Allow

# Create a new rule to allow outbound SMTP traffic on ports 25 and 587
foreach ($port in $SMTPRemotePorts) {
    New-NetFirewallRule -DisplayName "$AllowSMTPRuleName-$port" -Direction Outbound -Protocol $SMTPProtocol -RemotePort 
$port -Action Allow
}

# Check if the rules were successfully created
$allowHTTPSRule = Get-NetFirewallRule | Where-Object { $_.DisplayName -eq $AllowHTTPSRuleName }
$allowDNSRule = Get-NetFirewallRule | Where-Object { $_.DisplayName -eq $AllowDNSRuleName }
$allowSMTPRules = Get-NetFirewallRule | Where-Object { $_.DisplayName -like "$AllowSMTPRuleName-*" }

if ($allowHTTPSRule -and $allowDNSRule -and $allowSMTPRules.Count -eq $SMTPRemotePorts.Count) {
    Write-Host "Outbound HTTPS traffic is allowed, outbound DNS traffic is allowed, and outbound SMTP traffic is 
allowed on ports 25 and 587."
} else {
    Write-Host "Failed to create one or more of the rules."
}

